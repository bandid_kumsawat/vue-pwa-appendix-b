import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'
import Adddevive from './components/views//Thingsboard/Adddevive.vue'
// Import Views - Dash
// import DashboardView from './components/views/Dashboard.vue'
import TablesView from './components/views/Tables.vue'
import TasksView from './components/views/Tasks.vue'
import SettingView from './components/views/Setting.vue'
import AccessView from './components/views/Access.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'
import Thingsboardedit from './components/views/Thingsboard/Editer.vue'
import thingsboardmoniter from './components/views/Thingsboard/Monitor.vue'
import EMQ from './components/views/EMQ/Monitor.vue'
import Register from './components/views/Register/Register.vue'
import svgEditer from './components/views/svgEditer/Editer.vue'

// Routes
const routes = [
  {
    // not found handler
    path: '*',
    name: 'NotFoundView',
    component: NotFoundView,
    meta: {description: 'NotFoundView.'}
  },
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'svgEditer',
        component: svgEditer,
        name: 'svgEditer',
        meta: {description: 'svgEditer'}
      },
      {
        path: 'tables',
        component: TablesView,
        name: 'Tables',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'tasks',
        component: TasksView,
        name: 'Tasks',
        meta: {description: 'Tasks page in the form of a timeline'}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page'}
      }, {
        path: 'access',
        component: AccessView,
        name: 'Access',
        meta: {description: 'Example of using maps'}
      }, {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: true}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos'}
      }, {
        path: 'thingsboardedit',
        component: Thingsboardedit,
        name: 'Thingsboard',
        meta: { description: 'Thingboard Editer' }
      }, {
        path: 'thingsboardmoniter',
        alias: '/',
        // "alias" is path '/' route is here.
        component: thingsboardmoniter,
        name: 'Thingsboard',
        meta: { description: 'Moniter' }
      }, {
        path: 'EMQ',
        component: EMQ,
        name: 'EMQ',
        meta: { description: 'EMQ Moniter' }
      }, {
        path: 'Register',
        component: Register,
        name: 'Manage User',
        meta: {description: ''}
      }, {
        path: 'addDevice',
        component: Adddevive,
        name: 'Add Device',
        meth: {description: 'Add Device'}
      }
    ]
  }
]

export default routes
