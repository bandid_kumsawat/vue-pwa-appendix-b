import API from '@/service_api/server'

export default {
  signup (data) {
    return API().post('signup', data);
  }
}
// signup.signup({
//     name: 'บัณฑิต',
//     surname: 'คุ้มสวัสดิ์',
//     nickname: 'แชมป์',
//     age: '25',
//     address: '210 หมู่ 8 ต.สามพราน อ.สามพราน จ.นครปฐม',
//     phone: '0832216279',
//     email: 'bandid.kumsawat.facebook@hotmail.com',
//     pass: 'champ1234',
// })