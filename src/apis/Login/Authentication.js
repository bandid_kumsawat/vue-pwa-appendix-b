import API from '@/apis/server'

export default {
  Authen (data) {
    return API().post('users/login', data)
  }
}
