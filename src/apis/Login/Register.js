import API from '@/apis/server'

export default {
  user (data) {
    return API().post('users', data)
  },
  find (data, headers) {
    return API().post('/users/find', data, headers)
  },
  delete (data, headers) {
    return API().delete('/user/', headers)
  },
  update (data, headers) {
    return API().put('/user/', headers, data)
  }
}
