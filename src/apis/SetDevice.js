import axios from 'axios'

export default {
  setDevice (devicename) {
    var url = {
      baseURL: 'http://iotdma.info:1880/deviceid?device_id=' + devicename
    }
    return axios.get(url.baseURL)
  }
}
